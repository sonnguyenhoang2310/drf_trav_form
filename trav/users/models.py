from django.db import models
from django.contrib.auth.models import AbstractUser
from django.conf import settings
from django.utils.translation import ugettext_lazy as _


# Create your models here.
class Company(models.Model):
    name = models.CharField(max_length=100, unique=True)
    short_name = models.CharField(max_length=20, unique=True)
    website = models.CharField(max_length=200, unique=True)
    job_position = models.TextField()
    salary = models.TextField(default='')
    other_salary = models.TextField(default='')
    address = models.TextField()
    get_job_time = models.TextField()
    job_description = models.TextField()
    contact = models.TextField()
    created_time = models.DateTimeField(auto_now_add=True)
    delete_flag = models.BooleanField(default=False)
    phone_number = models.CharField(max_length=10, unique=True)

    class Meta:
        ordering = ('-id',)

    def __str__(self):
        return self.name


class User(AbstractUser):
    username = models.CharField(max_length=255, blank=True, null=True)
    email = models.EmailField(_('email address'), unique=True)

    class Meta:
        ordering = ('-id',)

    def __str__(self):
        return self.email

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']


class AppUser(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='profile')
    full_name = models.CharField(max_length=100)
    date_of_birth = models.DateField()
    identify_code = models.CharField(max_length=12, unique=True)
    weight = models.IntegerField()
    height = models.IntegerField()
    phone_number = models.CharField(max_length=20, unique=True)
    wanted_job = models.CharField(max_length=255, default='', blank=True, null=True)
    wanted_position = models.CharField(max_length=255, default='', blank=True, null=True)
    wanted_salary = models.CharField(max_length=255, default='', blank=True, null=True)
    degrees = models.CharField(max_length=255, default='', blank=True, null=True)
    worked_company = models.CharField(max_length=255, default='', blank=True, null=True)
    delete_flag = models.BooleanField(default=False)
    subscribed_company = models.ManyToManyField(Company, blank=True)
