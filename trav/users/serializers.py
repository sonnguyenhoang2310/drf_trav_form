from rest_framework import serializers
from users.models import User, AppUser, Company


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('__all__')


class CompanyMinimizeInformationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = ('id', 'name', 'short_name', 'job_description', 'phone_number', 'website')


class AppUserSerializer(serializers.ModelSerializer):
    subscribed_company = CompanyMinimizeInformationSerializer(read_only=True, many=True)

    class Meta:
        model = AppUser
        fields = ('full_name', 'date_of_birth', 'identify_code', 'weight', 'height', 'phone_number', 'wanted_job',
                  'wanted_position', 'wanted_salary', 'degrees', 'worked_company', 'subscribed_company')
        read_only_fields = ('identify_code', 'phone_number')


class UserSerializer(serializers.HyperlinkedModelSerializer):
    profile = AppUserSerializer(required=True)

    class Meta:
        model = User
        fields = ('id', 'password', 'email', 'profile')
        read_only_fields = ('email',)

    def create(self, validated_data):
        profile_data = validated_data.pop('profile')
        password = validated_data.pop('password')
        user = User(**validated_data)
        user.set_password(password)
        user.save()
        AppUser.objects.create(user=user, **profile_data)
        return user

    def update(self, instance, validated_data):
        profile_data = validated_data.pop('profile')
        profile = instance.profile

        profile.full_name = profile_data.get('full_name', profile.full_name)
        profile.date_of_birth = profile_data.get('date_of_birth', profile.date_of_birth)
        profile.weight = profile_data.get('weight', profile.weight)
        profile.height = profile_data.get('height', profile.height)
        profile.wanted_job = profile_data.get('wanted_job', profile.wanted_job)
        profile.wanted_salary = profile_data.get('wanted_salary', profile.wanted_salary)
        profile.degrees = profile_data.get('degrees', profile.degrees)
        profile.worked_company = profile_data.get('worked_company', profile.worked_company)
        profile.save()
        return instance


class UserResponseSerializer(UserSerializer):
    profile = AppUserSerializer(required=True)

    class Meta:
        model = User
        fields = ('id', 'email', 'profile')
