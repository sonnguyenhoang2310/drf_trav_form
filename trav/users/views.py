from django.contrib.auth import authenticate
# from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework import generics, mixins
from django.http import Http404, HttpResponse
from users.models import User, AppUser, Company
from users.serializers import UserSerializer, CompanySerializer, AppUserSerializer, UserResponseSerializer


def get_user(user_id):
    try:
        return User.objects.get(id=user_id)
    except User.DoesNotExist:
        raise Http404


def get_company(company_id):
    try:
        return Company.objects.get(id=company_id)
    except Company.DoesNotExist:
        raise Http404


# Create your views here.
@api_view(['POST'])
def subscribe(request):
    user_id = request.data['user_id']
    company_id = request.data['company_id']
    user = get_user(user_id)
    company = get_company(company_id)
    user.profile.subscribed_company.add(company.id)
    serializer = UserResponseSerializer(user)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['POST'])
def login(request):
    email = request.data['email']
    password = request.data['password']
    user = authenticate(email=email, password=password)
    if user is not None:
        app_user = User.objects.filter(email=email).first()
        serializer = UserResponseSerializer(app_user)
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response({"errors": "Username or password are incorrect"}, status=status.HTTP_401_UNAUTHORIZED)


class UserList(APIView):
    def get(self, format=None):
        users = User.objects.all()
        serializer = UserResponseSerializer(users, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserResponseSerializer


class CompanyList(generics.ListAPIView):
    queryset = Company.objects.all()
    serializer_class = CompanySerializer
